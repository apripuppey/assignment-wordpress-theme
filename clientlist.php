<?php
/*
Template Name: Client List
*/
get_header();


?>

<section>
    <div class="container">
        <h1 class="text-center"> Client List </h1>
        <hr/>
        <table class="table table-border table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

            
        <?php 

        $args = array(
            'post_type' => 'client',
            'posts_per_page' => -1
        );
        $query = new WP_Query($args);
        if ($query->have_posts() ) : 
            while ( $query->have_posts() ) : $query->the_post();
                
            echo "<tr>";
            echo "<td>".get_post_meta(get_the_ID(),'first_name',true)."</td>";
            echo "<td>".get_post_meta(get_the_ID(),'last_name',true)."</td>";
            echo "<td>".get_post_meta(get_the_ID(),'email',true)."</td>";
            echo "<td>".get_post_meta(get_the_ID(),'phone',true)."</td>";
            echo "<td>".get_post_meta(get_the_ID(),'address',true)  ."</td>";
            echo "<td>".get_post_meta(get_the_ID(),'city',true)  ."</td>";
            echo "<td><a href='javascript:void(0);' class='deleteClient' data-id='".get_the_ID()."' onclick='confirm_delete(this)'>Delete</a> | <a href='javascript:void(0);' class='editClient' data-toggle='modal' data-target='#editClientModal' data-id='".get_the_ID()."' >Edit</a></td>";
            
            echo "</tr>";

            endwhile;
            wp_reset_postdata();
        endif;

        ?>
            </tbody>
        </table>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="editClientModal" tabindex="-1" role="dialog" aria-labelledby="editClientModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editClientModalLabel">Edit Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editForm">
            <input type="hidden" name="id_client" id="id_client">
            <input type="hidden" name="action" value="update_client">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" name="first_name" id="first_name" class="form-control" required/>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" name="last_name" id="last_name" class="form-control" required />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" name="email" id="email" class="form-control" required />
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" id="phone" class="form-control" required/>
            </div>
            <div class="form-group">
                <label>Address</label>
                <textarea name="address" id="address" cols="30"  class="form-control" required></textarea>
            </div>
            <div class="form-group">
                <label>City</label>
                <select name="city" id="city" class="form-control">
                    <option value="Jakarta">Jakarta</option>
                    <option value="Hanoi">Hanoi</option>
                    <option value="Singapore">Singapore</option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Save changes</button>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
    var $ = jQuery;
    
    function confirm_delete(data){    
        var action_delete = confirm("Are you sure to delete this client?");
        if(action_delete){
            let id = $(data).attr('data-id');
            $.ajax({
                url: "<?php echo get_site_url();?>/wp-admin/admin-ajax.php",
                type: "POST",
                data: {client_id : id, action : 'delete_client'} ,
                success: function (response) {
                    var result = JSON.parse(response);
                    alert(result.message);
                    location.reload();

                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
            });
        }
    }

    $(document).on("click", ".editClient", function () {
        let id = $(this).data('id');
        $.ajax({
            url: "<?php echo get_site_url();?>/wp-admin/admin-ajax.php",
            type: "POST",
            data: {client_id : id, action : 'get_client'} ,
            beforeSend : function (){
                clearModal();
            },
            success: function (response) {
                
                var result = JSON.parse(response);
                
                $("#id_client").val(result.id_client);
                $("#first_name").val(result.first_name);
                $("#last_name").val(result.last_name);
                $("#email").val(result.email);
                $("#phone").val(result.phone);
                $("#address").val(result.address);
                $("#city").val(result.city);        
                

            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        });
    });

    $("#editForm").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);

        $.ajax({
            type: "POST",
            url: "<?php echo get_site_url();?>/wp-admin/admin-ajax.php",
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                var result = JSON.parse(data);
                alert(result.message);
                location.reload();
            }
        });


    });


    function clearModal(){
        $("#id_client").val('');
        $("#first_name").val('');
        $("#last_name").val('');
        $("#email").val('');
        $("#phone").val('');
        $("#address").val('');
        $("#city").val('');
    }

    

    
</script>
<?php 
get_footer();
?>

