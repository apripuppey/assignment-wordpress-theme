<?php 
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'assignment_wordpress' ),
));


function my_custom_theme_sidebar() {
    register_sidebar( array(
        'name' => __( 'Primary Sidebar', 'assignment_wordpress' ),
        'id'   => 'sidebar-1',
    ) );
}
add_action( 'widgets_init', 'my_custom_theme_sidebar' );


add_theme_support( 'post-thumbnails' );
add_image_size( 'my-custom-image-size', 640, 999 );


function my_custom_theme_enqueue() {
    wp_enqueue_style( 'assignment_wordpress', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri().'/vendor/bootstrap-4/css/bootstrap.min.css' );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri().'/vendor/bootstrap-4/js/bootstrap.min.js' );

}
add_action( 'wp_enqueue_scripts', 'my_custom_theme_enqueue' );


//fungsi untuk melakukan post ke CPT plugin dari Contact Form 7

function save_posted_data( $posted_data ) {
    
    $error_email = false; // set email flag false 

    $new_email = $posted_data['email'];

    // cek dulu apakah sudah ada data email yang sama
    $args = array(
        'post_type' => 'client',
        'posts_per_page' => -1
    );
    $query = new WP_Query($args);
    if ($query->have_posts() ) : 
        while ( $query->have_posts() ) : $query->the_post();
            $email = get_post_meta(get_the_ID(),'email',true);
            if($email == $new_email) : 
                $error_email = true;
            endif;

        endwhile;
        wp_reset_postdata();
    endif;

    // jika email error
    if($error_email){
        add_filter("wpcf7_validate", function ($result) {
                $result->offsetSet("reason", ["email" => "Email is already registered, please use another email"]);
            return $result;
        }, 10, 1);
    }else{
    // jika email sukses
        $args = array(
            'post_type' => 'client',
            'post_status'=>'publish',
            'post_title'=>$posted_data['email'],
            'first_name'=>$posted_data['first-name'],
            'last_name'=>$posted_data['last-name'],
            'email'=>$posted_data['email'],
            'phone'=>$posted_data['phone'],
            'address'=>$posted_data['address'],
            'city'=>$posted_data['city'][0]
        );
    
    
        $post_id = wp_insert_post($args);
    
    
        if(!is_wp_error($post_id)){
            
            // simpen ke masing2 custom field
            if( isset($posted_data['first-name']) ){
                update_post_meta($post_id, 'first_name', $posted_data['first-name']);
            }
            if( isset($posted_data['last-name']) ){
                update_post_meta($post_id, 'last_name', $posted_data['last-name']);
            }
            if( isset($posted_data['email']) ){
                update_post_meta($post_id, 'email', $posted_data['email']);
            }
            if( isset($posted_data['phone']) ){
                update_post_meta($post_id, 'phone', $posted_data['phone']);
            }
            if( isset($posted_data['address']) ){
                update_post_meta($post_id, 'address', $posted_data['address']);
            }
            if( isset($posted_data['city']) ){
                update_post_meta($post_id, 'city', $posted_data['city'][0]);
            }
            return $posted_data;
        }
    }




    
}

add_filter( 'wpcf7_posted_data', 'save_posted_data' );



function ajax_delete() {
    if(isset($_POST['client_id']))
    {   
        
        $delete=wp_delete_post($_POST['client_id']);
        if(!empty($delete)){
            echo json_encode(array("message" => "Client is deleted successfully"));
        }else{
            echo json_encode(array("message" => "Delete Failed, please try again later"));
        }
        die();
    } // end if
}

add_action('wp_ajax_delete_client', 'ajax_delete');
add_action('wp_ajax_nopriv_delete_client', 'ajax_delete');



function get_client() {
    if(isset($_POST['client_id']))
    {   
        
        $data = get_post_meta($_POST['client_id']);
        $data['id_client']=$_POST['client_id'];
        echo json_encode($data);
        die();
    } // end if
}

add_action('wp_ajax_get_client', 'get_client');
add_action('wp_ajax_nopriv_get_client', 'get_client');


function update_client() {
    
    if(isset($_POST['id_client']))
    {   
        $post_id=$_POST['id_client'];
        if( isset($_POST['first_name']) ){
            update_post_meta($post_id, 'first_name', $_POST['first_name']);
        }
        if( isset($_POST['last_name']) ){
            update_post_meta($post_id, 'last_name', $_POST['last_name']);
        }
        if( isset($_POST['email']) ){

            // validate email so the email will not double
            $old_email =  get_post_meta($post_id,'email',true);
            if($old_email !== $_POST['email']){
                //check if the email is exist on another record
                $new_email=$_POST['email'];
                $args = array(
                    'post_type' => 'client',
                    'posts_per_page' => -1
                );
                $query = new WP_Query($args);
                if ($query->have_posts() ) : 
                    while ( $query->have_posts() ) : $query->the_post();
                        $email = get_post_meta(get_the_ID(),'email',true);
                        if($email == $new_email) : 
                            $error_email = true;
                        endif;
            
                    endwhile;
                    wp_reset_postdata();
                endif;

                if(!$error_email){
                    update_post_meta($post_id, 'email', $new_email);
                }
            }
            
        }
        if( isset($_POST['phone']) ){
            update_post_meta($post_id, 'phone', $_POST['phone']);
        }
        if( isset($_POST['address']) ){
            update_post_meta($post_id, 'address', $_POST['address']);
        }
        if( isset($_POST['city']) ){
            update_post_meta($post_id, 'city', $_POST['city']);
        }

        echo json_encode(array("message" => "Client is updated successfully"));
        die();

    } // end if
}

add_action('wp_ajax_update_client', 'update_client');
add_action('wp_ajax_nopriv_update_client', 'update_client');


function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );



?>